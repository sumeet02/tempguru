import React, { Component } from "react";
import { render } from 'react-dom';
import {AuthContainer} from './components/container/AuthContainer';
import {MainContainer} from './components/container/MainContainer';
import { Provider } from 'react-redux';
import store from '../src/store';
import {BrowserRouter as Router,Route,Link,Switch} from 'react-router-dom';
let reactElement = document.getElementById('insightguru')
render(
	<Provider  store={store}>
	<Router>
	<Switch>
	<Route exact path='/' component={AuthContainer}/>
	<Route path='/dashboard' component={MainContainer}/>
	</Switch>
	</Router>
	</Provider>,
  reactElement 
)
