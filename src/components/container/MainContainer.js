import React from 'react';
import PanelContainer from './PanelContainer';
import Header from '../presentational/Header';
export class MainContainer extends React.Component{
	render(){
		return(
				<div>
				<Header/>
				<PanelContainer/>
				</div>
			);
	}
}