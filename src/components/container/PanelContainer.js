import React,{Component} from 'react';
import LeftComponent from '../presentational/LeftComponent';
import MiddleComponent from '../presentational/MiddleComponent';
import RightComponent from '../presentational/RightComponent';
import Grid from 'material-ui/Grid';
import GlobalStyles from '../../globalStyles';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
class PanelContainer extends Component{
	render(){
		 const {classes} = this.props;
		console.log(classes)
		return(
			 <main className={classes.content}>
   				<div className={classes.mainContainer}>
				<LeftComponent cls={classes}/>
				<MiddleComponent cls={classes}/>
				<RightComponent cls={classes}/>
			</div>
			</main>
			);
	}
}

 PanelContainer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(GlobalStyles)(PanelContainer);