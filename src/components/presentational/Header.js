import React from 'react';
import AppBar from 'material-ui/AppBar'
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Grid from 'material-ui/Grid';
const Header = () => (
	 <AppBar   color="inherit">
                  <Toolbar>
                     
                      <Grid container spacing={24}>
			   		<Grid item xs={10} sm={10} md={10} lg={10} xl={10}>
			   		<h6>insightguru</h6>
			   		</Grid>
			   		<Grid item xs={2} sm={2} md={2} lg={2} xl={2}>
			   		<h6>Swapnil@gmail.com</h6>
			   		</Grid>
			   		</Grid>
	
           </Toolbar>
         </AppBar>
	);

export default Header;