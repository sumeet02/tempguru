import React from 'react';
import Grid from 'material-ui/Grid';
import GlobalStyles from '../../globalStyles';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import classNames from 'classnames';
import Paper from 'material-ui/Paper';
import MoreVert from 'material-ui-icons/MoreVert';
import IconButton from 'material-ui/IconButton';
import Clear from 'material-ui-icons/Clear';
import NextIcon from 'material-ui-icons/ArrowForward';
import PreviousIcon from 'material-ui-icons/ArrowBack';
import BottomNavigation, { BottomNavigationAction } from 'material-ui/BottomNavigation';
import Keyboard from 'material-ui-icons/Keyboard';

const RightComponent = (cls) => {
	return <div className={cls.cls.rightPanel}>
   		<Grid item xs={12} sm={12} md={12} lg={12} xl={12} className={cls.cls.userCardHeaderContainer}>
                <Paper className={cls.cls.assesseesContainerGreen}>
                </Paper>
              </Grid>
   	</div>
};

export default  RightComponent;	