import React from 'react';
import Grid from 'material-ui/Grid';
import GlobalStyles from '../../globalStyles';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import classNames from 'classnames';
import Paper from 'material-ui/Paper';
import MoreVert from 'material-ui-icons/MoreVert';
import IconButton from 'material-ui/IconButton';
import Clear from 'material-ui-icons/Clear';
import NextIcon from 'material-ui-icons/ArrowForward';
import PreviousIcon from 'material-ui-icons/ArrowBack';
import BottomNavigation, { BottomNavigationAction } from 'material-ui/BottomNavigation';
import Keyboard from 'material-ui-icons/Keyboard';

const LeftComponent = (cls) => {
	console.log(cls)
	return <div className={cls.cls.leftPanel}>
   		<Grid item xs={12} sm={12} md={12} lg={12} xl={12} className={cls.cls.userCardHeaderContainer}>
                <Paper className={cls.cls.assesseesContainer}>
                	<div className={cls.cls.moreTextPanelHeader}>
                	<div>
                		 <span>dashboard</span>
                		 </div>
                	</div>
                	<div className={cls.cls.iconBox}>
                                    <IconButton>
                                    <PreviousIcon className={cls.cls.iconsBarDefault}/></IconButton>
                                </div>
                                <div className={cls.cls.iconBox}>
                                     <IconButton ><MoreVert className={cls.cls.iconsBarDefault}/></IconButton>
                     </div>
                </Paper>
                 <div style={{height: 460, overflow: 'overlay'}}>
                 	 <div
                                            style={
                                                {
                                                    display: 'flex',
                                                    flexDirection: 'column',
                                                    height: 460,
                                                    justifyContent: 'space-between'
                                                }
                                            }
                                        >
                                       <div style={{height: 460, overflow: 'overlay', marginTop: 10}}>
                                        <div className={cls.cls.dossierContainerProfile}>
                                        	<div>
                                        		<Paper className={cls.cls.dashboardCardTop} style={{cursor: 'initial',backgroundColor: '#fff'}}>
                                        			 <div className={classNames(cls.cls.dashboard, cls.cls.fillRight)}>
									                    <div className={cls.cls.dashboardlabelName}>
										                    Swapnil@gmail.com
									                    </div>
                									</div>
                                        		</Paper>
                                        	</div>

                                        </div>
                 		</div>
                 		  <div className={cls.cls.leftFooterD}>
                                                <div className={cls.cls.footerInner}>
                                              			<Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
                                              				<BottomNavigation
										                     
										                        className={cls.cls.MuiBottomNavigationCustom}
										                    >
										                    <div>
										                        <Keyboard />
										                    </div>
										                    </BottomNavigation>
                                              			</Grid>
                                                </div>
                                            </div>

                 		</div>
                   </div>
                  
              </Grid>
   	</div>

	};


export default  LeftComponent;	